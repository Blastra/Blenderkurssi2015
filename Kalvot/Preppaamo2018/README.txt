Tämä kansio sisältää lisäkalvot, jotka tuotettiin repositorioon valmisteluna Seinäjoella Preppaamo-hankkeessa tapahtuvaan opetukseen vuonna 2018.

Mikäli Sepelin sivut eivät ole menneet nurin, https://sepeliry.github.io/ :sta löytyy materiaalia Blender -mallinnuksen perusharjoituksista.

